CC	   = gcc
FLAGS  = -Wall -O3   -Wno-unused-result  -Wno-unused-variable -Wno-unused-but-set-variable
LIBS   = -lm #-fopenmp
GSL    = -I/usr/include -c
GSLLIB = -lgsl -lgslcblas
DEPS = functions.h
OBJ = main.o functions.o
EXEC = emri

all: $(EXEC)


emri: main.o functions.o 
	$(CC) $(LPATH) $?  $(GSLLIB) -o $@ $(LIBS)
main.o: main.c 
	$(CC) $(FLAGS) $(GSL) -o $@ $? $(LIBS)
functions.o: functions.c 
	$(CC) $(FLAGS) $(GSL) -o $@ $? $(LIBS)


clean:
	rm -f *.o .m* .n* *.lst *.L *~ $(EXEC) *# 

