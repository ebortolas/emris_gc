#include "functions.h"
# include <stdio.h>      //input/output
# include <stdlib.h>    //standard library
# include <math.h>       //mathematic func library
# include <time.h>       //seed random numbers
# include <string.h>
#include <unistd.h>      //for host name


# include <gsl/gsl_rng.h>          //random numbers
# include <gsl/gsl_sf.h>          //special funcs
# include <gsl/gsl_roots.h>
# include <gsl/gsl_errno.h>        //error output

# define sq(x) ((x)*(x))

# define MSCALE (1.e-6) 
# define RSCALE_AU (4.84814e-3)
# define RSCALE_PC (1000.)
# define MMIN1 (9.0*MSCALE) //minimum mass (9 Msun) in units of 10^6 solar masses for the future supernova
# define MMAX (100.0*MSCALE)//(18.8811*MSCALE) would be for NSs //maximum mass mass in units of 10^6 solar masses

# define MBH (4.3)         //BH mass in units of 10^6 solar masses
# define GAUS_MEAN (0.3)
# define GAUS_SIGMA (0.1)
# define R_CWD_MIN (0.001*RSCALE_PC) //internal radius of clockwise disk (0.001 pc) in units of 10^-3 pc
# define R_CWD_MAX (0.13*RSCALE_PC) //external radius of clockwise disk (0.13 pc) in units of 10^-3 pc
# define GAMMA_CWD (0.93)

# define SIGMA_MAX (265./V_UNIT_SCALED) //SIGMA of maxwellian for velocity kick, equiv 265 km/s


# define fact (180./M_PI)
# define PREC (1.e-5)
# define NMAX (50000)

# define Gamma (0.1)
# define Mcusp (0.)
# define R0 (1000.)

# define Ngrid (360)

# define t_Hubble (1.4e+10) // Hubble time in yr


#define Rstrip (3.*2.255e-5)   //stripping radius for tdes in millipc

# define TRUE (1)
# define FALSE (0)


void now(long int seed)
{
  // Time and date!
  time_t curtime;
  struct tm *loctime;
  curtime = time (NULL);//Get the current time
  loctime = localtime (&curtime);// Convert it to local time representation
  fputs ("# ", stdout); 
  fputs (asctime (loctime), stdout);  // Print date+time, standard format
  
  char hostname[1024];
  gethostname(hostname, 1024);
  printf("# Random number generator seed on %s: %li\n#\n", hostname,seed);
  fflush(stdout);
  return;
}


double mass_generator(double Mmin, double Mmax, double Mslope, double rand)
{
  double Norm=( pow(Mmin, -Mslope)  -  pow(Mmax, -Mslope))/Mslope; //Normalization constant
  double mass=pow(pow(Mmin, -Mslope)-Mslope*Norm*rand, -1./Mslope); //MC transformation method
  return (mass);
}

void binary_generator(unsigned long int it, double m1, double m2, double e, double a, double trand, double *pos1, double *vel1, double *pos2, double *vel2,  double *period, double *f0)
{

  #ifdef DEBUG
  printf("Generating unperturbed binary system (remn+bh)....\n");
  #endif
  double mtot;
  double T,t;
  double n; //mean motion
  double M=0.; //mean anomaly
  double E,E0; // eccentric anomaly
  double f; // true anomaly
  double r;//, rp,ra; // radius
  double vrad,vtan;
  double delta;
  //double  Ekin, Epot, Etot;
  //reduced particle
  double xx,yy,vxx,vyy;
  //Two particles
  double x1,y1,z1;
  double x2,y2,z2;
  double vx1,vy1,vz1;
  double vx2,vy2,vz2;
  mtot=m1+m2;
  //rp=a*(1.-e);
  //ra=a*(1.+e);
  T=2.*M_PI*sqrt(a*a*a/(mtot));
  *period=T;
  n=2.*M_PI/T;
  t=trand*T; 
  M=n*t;    
  int iter=0;
  E=M;
  do
  {
    E0=E;
    E=M+e*sin(E);
    delta=fabs((E-E0)/E);
    iter=iter+1;
    if(iter>NMAX)
    {
      fprintf(stderr, "#   WARNING (%lu)!   Maximum number of iterations %d ! delta = %.2g\n",it,NMAX,delta);
      break;
    }
  }while(delta>PREC);
  f=2.*atan(tan(.5*E)*sqrt((1.+e)/(1.-e)));
  *f0=M;
  r=a*(1.-e*e)/(1.+e*cos(f));
  vrad=2.*M_PI*a*e*sin(f)/(T*sqrt((1.-e*e)));
  vtan=2.*M_PI*a*(1.+e*cos(f))/(T*sqrt(1.-e*e));
  
  // reduced particle
  xx=r*cos(f); 
  yy=r*sin(f);
  vxx=vrad*cos(f)-vtan*sin(f);
  vyy=vrad*sin(f)+vtan*cos(f);
  //Real particles
  x1=-m2*xx/mtot;
  y1=-m2*yy/mtot;
  z1=0.;
  x2=m1*xx/mtot;
  y2=m1*yy/mtot;
  z2=0.;
  
  vx1=-m2*vxx/mtot;
  vy1=-m2*vyy/mtot;
  vz1=0.;
  vx2=m1*vxx/mtot;
  vy2=m1*vyy/mtot;
  vz2=0.;

  #ifdef DEBUG 
  printf("   Assigning positions and velocities...");
  #endif 
  
  pos1[0]=x1;  pos1[1]=y1;  pos1[2]=z1;
  vel1[0]=vx1; vel1[1]=vy1; vel1[2]=vz1;
  pos2[0]=x2;  pos2[1]=y2;  pos2[2]=z2;
  vel2[0]=vx2; vel2[1]=vy2; vel2[2]=vz2;  
  
  #ifdef DEBUG 
  printf(" done!\n");
  #endif 
  
  return;
}


void read_masses(FILE *snexpl, FILE *fradii, double *tfin, double *mzams, double *mfin, double *mremn, double *rmax)
{
  #ifdef DEBUG
  printf("Reading mass table.....\n");
  #endif
  
  int i, j,n;
  int N=Ngrid, M=100;
  if(!snexpl || ! fradii)
  {
    fprintf(stderr,"   Miss snexpl or radii file\n");
    exit(EXIT_FAILURE);
  }
  for(i=0; i<N; i++) 
  {
    fscanf(snexpl,"%lf %lf %lf %lf ", &tfin[i], &mzams[i], &mfin[i], &mremn[i]);
  }

  double rval[N][M];
  //matrix of radii
  for(i=0;i<N;i++)
  {
    for (j=0;j<M;j++)
    {
      rval[i][j]=-1.;
    }
  }
  
  i=j=n=0;
  char * strinm= malloc((4096) * sizeof(char));
  char *p;
  
  for (i=0; i<N; i++)
  {
    fgets(strinm, 4000, fradii);
    p=strinm;
    j=0;
    while(sscanf( p, "%lf  %n", &rval[i][j], &n ) == 1 )
    {
      p+=n;
      j=j+1;
    }
  }
  free(strinm);
  
  for(i=0;i<N;i++)
  {
    rmax[i]=rval[i][0];
    for (j=0;j<M;j++)
    {
      if(rval[i][j]>rmax[i])
        rmax[i]=rval[i][j];     
    }
  }
  
  for(i=0;i<N;i++)
    rmax[i]*=2.255e-5; //conversion from solar radii to millipc
  
  return;  
}


void change_masses(double *tfin, double *mzams, double *mfin, double *mremn, double m1i, double *rmax, double *m1presn, double *m1f, double *Tsn, double *thismaxradius)
{ 
  #ifdef DEBUG
  printf("Generating mass evolution......\n");
  #endif
  
  int i, N=Ngrid;
  double delta1, delta2;
  double tsn;
  
  for(i=0; i<N; i++)
  {
    if (m1i/MSCALE<=mzams[i])
      break;
    if(i==N-1)
    {
      fprintf(stderr,"   Error! tfin_mzams_mfin_mrem_delayed.dat COMPLETELY SCANNED\n");
      exit(EXIT_FAILURE);
    }
  }
  if(i<18)
  {
      fprintf(stderr,"   Error! M1 cannot undergo supernova, check mass limits\n");
      exit(EXIT_FAILURE);
  }
  #ifdef DEBUG
  printf("   m1i is included between %lf < %lf < %lf\n",mzams[i-1],m1i/MSCALE,mzams[i]);
  #endif
  delta1 = mzams[i] - m1i/MSCALE;
  delta2 = m1i/MSCALE-mzams[i-1];
  tsn=(tfin[i-1]*delta1+tfin[i]*delta2)/(delta1+delta2);
  
  if(rmax[i-1]>rmax[i])
    *thismaxradius=rmax[i-1];
  else
    *thismaxradius=rmax[i];
    
  *m1f=(mremn[i-1]*delta1+mremn[i]*delta2)/(delta1+delta2);
  (*m1f)=(*m1f)*MSCALE;
  #ifdef DEBUG
  printf("   Supernova explodes betw: %lf Myr> **%lf** > %lf Myr\n",tfin[i-1],tsn,tfin[i]);
  # endif
  (*Tsn)=tsn;
  
  *m1presn=(mfin[i-1]*delta1+mfin[i]*delta2)/(delta1+delta2);
  (*m1presn)=(*m1presn)*MSCALE;
  
  #ifdef DEBUG
  printf("   Mass of m1 before supernova       : %lf < %lf < %lf\n",mfin[i-1],*m1presn/MSCALE,mfin[i]);
  printf("   Mass of m1 after  supernova (remn): %lf < %lf < %lf\n",mremn[i-1],*m1f/MSCALE,mremn[i]);
  printf("   Maximum radius: %lf mpc = %lf solar radii\n",*thismaxradius,*thismaxradius/2.255e-5 );
  #endif
  return;  
}

double CDFmax_f(double u, void *pp)
{
  struct funz_p1 * params = (struct funz_p1 *) pp;
  double F=(params->F);
  return  F-gsl_sf_erf(u)+2.*u*exp(-u*u)/(sqrt(M_PI));
}

double CDFmax_df(double u,void *pp)
{
  return  -4.*u*u*exp(-u*u)/(sqrt(M_PI));
}

void CDFmax_fdf(double u, void *pp, double *y, double *dy)
{
    *y = CDFmax_f(u, pp);
    *dy = CDFmax_df(u, pp);
}


double generate_kickvel(double sigma, double rand1 , double rand2 /*rand*/)
{
  int i=0, max_i=50000, status;
  double tol=1.e-8;
  double F=rand1;
  double u0=1.,u=1.;
  struct funz_p1 PAR = { F };
  
  // Secant newton method to find the zero
  const gsl_root_fdfsolver_type *T;
  gsl_root_fdfsolver *s;
  gsl_function_fdf FDF;
  FDF.f = &CDFmax_f;
  FDF.df = &CDFmax_df;
  FDF.fdf = &CDFmax_fdf;
  FDF.params = &PAR;
  T = gsl_root_fdfsolver_newton;
  s = gsl_root_fdfsolver_alloc (T);
  
  gsl_root_fdfsolver_set (s, &FDF, u);

  do
  {
    i=i+1;
    status = gsl_root_fdfsolver_iterate(s);
    u0 = u;
    u = gsl_root_fdfsolver_root (s);
    status = gsl_root_test_delta (u, u0, 0, tol);
  }while (status == GSL_CONTINUE && i < max_i);  
  gsl_root_fdfsolver_free (s);
  return (sqrt(2.*sigma*sigma*u*u));
  
}

double gaussian(double x, double mean, double sigma)
{
  return (exp(-(x-mean)*(x-mean)/(2.*sigma*sigma))/(sqrt(2.*M_PI)*sigma));
}

double generate_gaussian_eccentricity_distr(gsl_rng * randnum, double mean, double sigma)
{
  double Max=1./(sqrt(2.*M_PI)*sigma);
  double x, gaus_val=0., try=1000.;
  x=-100.;
  int i=0;
  while(try>=gaus_val)
  {
    x=gsl_rng_uniform(randnum);
    gaus_val=gaussian(x,mean,sigma);
    try=Max*gsl_rng_uniform(randnum); 
    i++;
  }
   return(x);  

}

double generate_cwd_radial_distribution(double rmin, double rmax, double rslope, double rand)
{
  double tot_slope=2.-rslope;
  double Norm=( pow(rmax,tot_slope)  -  pow(rmin, tot_slope))/tot_slope; //Normalization constant
  double r=pow( tot_slope*Norm*rand + pow(rmin, tot_slope), 1./tot_slope); //MC transformation method
  return (r);
}

double generate_powerlaw_eccentricity_distr(double alpha, double rand)
{
  if(alpha>-1.)
    return pow(rand, 1./(alpha+1.));
  else
  {
    fprintf(stderr, "Invalid alpha value for eccentricity distribution!\nExit...\n");
    exit(EXIT_FAILURE);
  }

}

double generate_cuspy_profile(double rmin, double rmax, double gamma, double rand)
{
  double Norm; //Normalization constant
  if(gamma==3.0) 
    Norm = log(rmax/rmin);
  else
    Norm=( pow(rmax,3.-gamma)  -  pow(rmin, 3.-gamma))/(3.-gamma); 
  
  double r;
  if(gamma==3.0) 
    r=rmin*exp(rand*Norm);
  else
    r=pow( (3.-gamma)*Norm*rand + pow(rmin, 3.-gamma), 1./(3.-gamma)); 
  
  return (r);
}



void rotate(double *vec, double psi, double theta, double phi)
{
  int i,j,k;
  double tr1[3][3]; //Z1
  tr1[0][0]=cos(phi);  tr1[0][1]=sin(phi);    tr1[0][2]=0.;
  tr1[1][0]=-sin(phi); tr1[1][1]=cos(phi);    tr1[1][2]=0.;
  tr1[2][0]=0.;        tr1[2][1]=0.;          tr1[2][2]=1.;
  
  double tr2[3][3]; //X
  tr2[0][0]=1.;        tr2[0][1]=0.;          tr2[0][2]=0.;
  tr2[1][0]=0.;        tr2[1][1]=cos(theta);  tr2[1][2]=sin(theta);
  tr2[2][0]=0.;        tr2[2][1]=-sin(theta); tr2[2][2]=cos(theta);
  
  double tr3[3][3]; //Z2
  tr3[0][0]=cos(psi);  tr3[0][1]=sin(psi);    tr3[0][2]=0.;
  tr3[1][0]=-sin(psi); tr3[1][1]=cos(psi);    tr3[1][2]=0.;
  tr3[2][0]=0.;        tr3[2][1]=0.;          tr3[2][2]=1.;
  
  double prod[3][3];
  
  for (i = 0; i <3; i++) 
  {
    for (j = 0; j <3; j++) 
    {
      prod[i][j]=0.;
      for (k = 0; k <3; k++) 
        prod[i][j]+= tr2[i][k] * tr1[k][j];
    }
  }
  
  double matr[3][3];
  
  for (i = 0; i <3; i++) 
  {
    for (j = 0; j <3; j++) 
    {
      matr[i][j]=0.;
      for (k = 0; k <3; k++) 
        matr[i][j]+= tr3[i][k] * prod[k][j];
    }
  }
  
  double rot_vec[3];
  for (i = 0; i <3; i++) 
  {
    rot_vec[i]=0.;
    for (k = 0; k <3; k++) 
      rot_vec[i]+= matr[i][k] * vec[k];
  }
  
  for(i=0; i<3;i++)
    vec[i]=rot_vec[i];
  
  return;
  
}

double fgw(double e)
{ 
  double f=(pow((1.-e*e),3.5))/(1. + 73./24.*e*e + 37./96.*(e*e*e*e));
  return f;
}

double TGW (double a, double e, double M, double m) 
{
   double c=2.99792458e+5; //in km/s
   double g=4.302e-3; //in pc/Msun*(km/s)**2
   double lmks=3.0857e+13;  //  1 pc   in km
   double tmks=3.1557600e+7; // 1 yr   in s
   double t=(fgw(e)*(5./64.)*(c*c*c*c*c)*(a*a*a*a) /(g*g*g * M*M*m))*lmks;
   return (t/tmks); // returns the GW infall timescale in YR!!
}

double angle_between_vectors(double *a, double *b)
{
  //return the angle in radiants between two vectors
  int i,Ndim=3;
  double mod_a=0.,mod_b=0., scal_prod=0.;
  for(i=0; i<Ndim; i++)
  {
    mod_a+=sq(a[i]);
    mod_b+=sq(b[i]);
    scal_prod+=a[i]*b[i];
  }
  mod_a=sqrt(mod_a);
  mod_b=sqrt(mod_b);
  
  return acos(scal_prod/(mod_a*mod_b));
}

int plunge(double a, double e, double mbh)
{
  // a in pc, mbh in Msun
  double c=2.99792458e+5; //in km/s
  double g=4.302e-3; //in pc/Msun*(km/s)**2
  if ( (a*(1.-e)) <6.*g*mbh /(c*c) )
    return TRUE;
  else
    return FALSE;
}

int NRR_deflection(double a, double e, double mbh, 
                   double gamma, double C_gamma,
                   double mstar, double nstar)
{
  // a in pc, mbh in Msun
  // if my computation is in pc, nstar should be the number of stars within 1 pc
  double c=2.99792458e+5; //in km/s
  double g=4.302e-3; //in pc/Msun*(km/s)**2
  
  double val = 6.*sqrt(2.)*M_PI*M_PI/5./C_gamma;
  val *= ((1.-e*e)/fgw(e));
  val *= (mbh/mstar);
  val /= (nstar * log(0.5*mbh/mstar) );
  val *=pow((g*mbh/(c*c)), 5./2.); 
  double acheck = pow(val, 1./(-gamma+(11./2.)));
  if(a>acheck)
    return TRUE;
  else
    return FALSE;
}


