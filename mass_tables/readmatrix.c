# include <stdio.h>      //input/output
# include <stdlib.h>    //standard library
# include <math.h>       //mathematic func library
# include <time.h>       //seed random numbers
# include <string.h>

void matrices(char *path_mtables, int m);


int main (int argc, char *argv[])
{ 

  double m=atof(argv[1]);
  char st[]="/home/bortolas/Dropbox/PhD/dark_remnants_IC/IC/mass_tables";
  
  matrices(st,m);
  return 0;
}

void matrices(char *path_mtables, int m)
{ 
  
  int i,j,n;
  char *p;
  int N=360,M=100;
  double tfin[N], mzams[N], mfin[N], mremn[N], tevolve[N][M],mevolve[N][M], revolve[N][M];
  double delta1, delta2;
  char strin[500];
  
  FILE *massmatrix;
  strcpy(strin, path_mtables);
  strcat(strin, "/MassMatrix.dat");
  massmatrix = fopen (strin, "r");
  if(!massmatrix)
  {
    fprintf(stderr,"Miss %s\n",strin);
    exit(EXIT_FAILURE);
  }
  FILE *timematrix;
  strcpy(strin, path_mtables);
  strcat(strin, "/TimeMatrix.dat");
  timematrix = fopen (strin, "r");
  if(!timematrix)
  {
    fprintf(stderr,"Miss %s\n",strin);
    exit(EXIT_FAILURE);
  }
  
  FILE *radiusmatrix;
  strcpy(strin, path_mtables);
  strcat(strin, "/RadiusMatrix.dat");
  radiusmatrix = fopen (strin, "r");
  if(!radiusmatrix)
  {
    fprintf(stderr,"Miss %s\n",strin);
    exit(EXIT_FAILURE);
  }
  
  for(i=0;i<N;i++)
  {
    for (j=0;j<M;j++)
    {
      mevolve[i][j]=tevolve[i][j]=revolve[i][j]=-1.;
    }
  }
  
  i=j=n=0;
  char * strinm= malloc((4096) * sizeof(char));
  for (i=0; i<360; i++)
  {
    fgets(strinm, 4000, massmatrix);
    p=strinm;
    j=0;
    while(sscanf( p, "%lf  %n", &mevolve[i][j], &n ) == 1 )
    {
      p+=n;
      j=j+1;
    }
  }
  free(strinm);
  fclose(massmatrix);

  
  i=j=n=0;
  char * strinw= malloc((4096) * sizeof(char));
  for (i=0; i<360; i++)
  {
    fgets(strinw, 4000, timematrix);
    p=strinw;
    j=0;
    while(sscanf( p, "%lf  %n", &tevolve[i][j], &n ) == 1 )
    {
      p+=n;
      j=j+1;
    }
  }
  free(strinw);
  fclose(timematrix);
  
  i=j=n=0;
  char * strinr= malloc((4096) * sizeof(char));
  for (i=0; i<360; i++)
  {
    fgets(strinr, 4000, radiusmatrix);
    p=strinr;
    j=0;
    while(sscanf( p, "%lf  %n", &revolve[i][j], &n ) == 1 )
    {
      p+=n;
      j=j+1;
    }
  }
  free(strinr);
  fclose(radiusmatrix);
  
  //primo indice (i) se varia stampi una colonna
  //secondo indice (j) se varia stampi una riga
  for (j=0;mevolve[m][j]>0.;j++)
    printf("%14.9e  %14.9e  %14.9e  \n", tevolve[m][j],mevolve[m][j],revolve[m][j]);
  

  return;  
}
