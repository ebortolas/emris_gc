# include <stdio.h>      //input/output
# include <stdlib.h>    //standard library
# include <math.h>       //mathematic func library
# include <time.h>       //seed random numbers
# include <string.h>
#include <unistd.h>      //for host name


# include <gsl/gsl_rng.h>          //random numbers
# include <gsl/gsl_sf.h>          //special funcs
# include <gsl/gsl_roots.h>
# include <gsl/gsl_errno.h>        //error output


struct funz_p1 { double F; };


struct funz_pp
{
  double gamma;
  double M; 
  double mbh;
  double r0;
  double E;
  double L;
};



void now(long int seed);

double mass_generator(double Mmin, double Mmax, double Mslope, double rand);

void binary_generator(unsigned long int it, double m1, double m2, double e, double a, double trand, double *pos1, double *vel1, double *pos2, double *vel2,  double *period, double *f0);

void read_masses(FILE *snexpl, FILE *fradii, double *tfin, double *mzams, double *mfin, double *mremn, double *rmax);

void change_masses(double *tfin, double *mzams, double *mfin, double *mremn, double m1i, double *rmax, double *m1presn, double *m1f, double *Tsn, double *thismaxradius);

double CDFmax_f(double u, void *pp);

double CDFmax_df(double u,void *pp);

void CDFmax_fdf(double u, void *pp, double *y, double *dy);

double generate_kickvel(double sigma, double rand1 , double rand2 /*rand*/);

double gaussian(double x, double mean, double sigma);

double generate_gaussian_eccentricity_distr(gsl_rng * randnum, double mean, double sigma);

double generate_cwd_radial_distribution(double rmin, double rmax, double rslope, double rand);

double generate_powerlaw_eccentricity_distr(double alpha, double rand);

double generate_cuspy_profile(double rmin, double rmax, double gamma, double rand);

void rotate(double *vec, double phi, double theta, double psi);

double fgw(double e);

double TGW (double a, double e, double M, double m);

double angle_between_vectors(double *a, double *b);

int plunge(double a, double e, double mbh);

int NRR_deflection(double a, double e, double mbh, double gamma, double C_gamma, double mstar, double nstar);

