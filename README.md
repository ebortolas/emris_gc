############# Author and Credit: Elisa Bortolas
############# First release: August 30th, 2018

*** Monte carlo code to study the EMRI rate in the
 *  Galactic Centre as a result of Supernova Kicks.
 *  Most of the code uses N-body units:
 *  G = 1, L = 1 mpc, M = 10^6 Msun

