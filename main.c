# include <stdio.h>      //input/output
# include <stdlib.h>    //standard library
# include <math.h>       //mathematic func library
# include <time.h>       //seed random numbers
# include <string.h>
# include <unistd.h>      //for host name
// #include <omp.h> 

# include <gsl/gsl_rng.h>          //random numbers
# include <gsl/gsl_sf.h>          //special funcs
# include <gsl/gsl_roots.h>
# include <gsl/gsl_errno.h>        //error output
#include "functions.h"

# define sq(x) ((x)*(x))

# define MASS_UNIT (1.98855e+36) // 10^6Msun in kg
# define LENGTH_UNIT (3.0857e+13) //  10^-3 pc in meters
# define G0 (6.67408e-11)  //G in mks

# define G (1.)

# define T_UNIT sqrt(LENGTH_UNIT*LENGTH_UNIT*LENGTH_UNIT/(G0*MASS_UNIT))
# define V_UNIT sqrt(G0*MASS_UNIT/LENGTH_UNIT)
# define V_UNIT_SCALED (sqrt(G0*MASS_UNIT/LENGTH_UNIT)/1000.)

# define MSCALE (1.e-6) 
# define RSCALE_AU (4.84814e-3)
# define RSCALE_PC (1000.)
# define MMIN1 (9.0*MSCALE) //minimum mass (9 Msun) in units of 10^6 solar masses for the future supernova
# define MMAX (100.0*MSCALE)//(18.8811*MSCALE) would be for NSs //maximum mass mass in units of 10^6 solar masses

# define MBH (4.3)         //BH mass in units of 10^6 solar masses
# define GAUS_MEAN (0.3)
# define GAUS_SIGMA (0.1)
# define R_CWD_MIN (0.001*RSCALE_PC) //internal radius of clockwise disk (0.001 pc) in units of 10^-3 pc
# define R_CWD_MAX (0.13*RSCALE_PC) //external radius of clockwise disk (0.13 pc) in units of 10^-3 pc
# define GAMMA_CWD (0.93)

# define SIGMA_MAX (265./V_UNIT_SCALED) //SIGMA of maxwellian for velocity kick, equiv 265 km/s

# define Gamma (0.1)
# define Mcusp (0.)
# define R0 (1000.)

# define Ngrid (360)

# define t_Hubble (1.4e+10) // Hubble time in yr

#define Rstrip (3.*2.255e-5)   //stripping radius for tdes in millipc

# define TRUE (1)
# define FALSE (0)

# define IMAX (1000000)


int main (int argc, char *argv[])
{ 
  unsigned long int Iend=10;//, Iprint, I_printed_tot;
  unsigned long int I;
  
  char buffer[256],*path_mtables;
  
  double tfin[Ngrid], mzams[Ngrid], mfin[Ngrid], mremn[Ngrid], rmax[Ngrid];
   
  //Random number generation initialized
  const gsl_rng_type * Type;
  long int seed;
  gsl_rng * randnum;
  gsl_rng_env_setup();
  Type = gsl_rng_ranlxd2;
  randnum = gsl_rng_alloc (Type);
  seed=2;//time(NULL);
  gsl_rng_set(randnum,seed);
  
    
  now(seed); // print date and host machine
  
  //initial conditions
  double rmin_ic=0.001*RSCALE_PC;
  double rmax_ic=3.5*RSCALE_PC;
  double gamma=1.1;
  double alpha_ecc= 1.0;
  double imf_slope=1.7;
  int norm_bh_kick=FALSE;
  
  /******************* MASS TABLES READING AND STORING *****************/
  FILE *snexpl;
  strcpy(buffer, "./mass_tables");
  strcat(buffer, "/tfin_mzams_mfin_mrem_delayed.dat");
  snexpl = fopen (buffer, "r");
  if(!snexpl)
  {
    fprintf(stderr,"   Miss snexpl\n");
    exit(EXIT_FAILURE);
  }
  
  FILE *fradii;
  strcpy(buffer, "./mass_tables");
  strcat(buffer, "/RadiusMatrix.dat");
  fradii = fopen (buffer, "r");
  if(!fradii)
  {
    fprintf(stderr,"   Miss fradii\n");
    exit(EXIT_FAILURE);
  }

  read_masses(snexpl, fradii, tfin, mzams, mfin, mremn, rmax);

  fclose(snexpl);
  fclose(fradii);
  
  /************************ end mass tables part ***********************/  
  
  
  
  
  
  /*********** below we adopt nbody units G=1, M=10^6 msun, L=1 mpc ***********/
  /*
  printf("#Mass   unit = %8e kg = %8e M_sun\n",MASS_UNIT, MASS_UNIT/1.98855e+30);
  printf("#Length unit = %8e m  = %8e AU = %8e pc\n",
               LENGTH_UNIT, LENGTH_UNIT/1.4960e+11,LENGTH_UNIT/3.0857e+16);
  printf("#Time   unit = %8e s  = %8e hr = %8e days = %8e yr\n",
         T_UNIT, T_UNIT/3600., T_UNIT/(3600.*24.), T_UNIT/(3600.*24.*365.));
  printf("#Speed  unit = %8e km/s\n#\n", V_UNIT_SCALED);
   */  
  printf("#Assumed radii ranging from %.3lf pc to %.3lf pc, rho(r) = r^-%.3lf\n",
                rmin_ic/RSCALE_PC,rmax_ic/RSCALE_PC,gamma);
  printf("#Assumed Eccentricity distribution: power-law e^alpha, alpha = %.2lf\n",
                alpha_ecc);
  printf("#Normalize bh kick = %d (1=true, 0=false) vk = %5.3f; smbh mass = %5.3g ;\n",
            norm_bh_kick,SIGMA_MAX*V_UNIT_SCALED,MBH/MSCALE);
  printf("#imf slope: %4.2lf between %5.2f and %5.2f\n",
                imf_slope, MMIN1/MSCALE, MMAX/MSCALE);
 
  
  /***************** RUN at DIFFERENT SEMIM AXIS ******************************/
  // first, create an a array
  int nbin=20,ib;
  double lgmin=log(rmin_ic), lgmax=log(rmax_ic); //in mpc
  double lgstep = (lgmax-lgmin)/((double)(nbin-1));
  double array[nbin];
  for(ib=0;ib<nbin;ib++)
  {
    array[ib]=exp(lgmin+((double)ib)*lgstep);
  }
  
  int imax=IMAX;
    if(argc>1)
      imax=atoi(argv[1]);


  printf("# R(0)   tde(1)     strip(2)   premat(3)  unbond(4)  plunge(5)  st_plng(6) Hemris(7)  st_Hemr(8) Lemris(9)  st_Lemr(10) Ntot(11)\n");  

  //#pragma omp parallel for 
  for(ib=0;ib<nbin;ib++)
  { 
    //variables for computation   
    double m1i, m1presn, m1f;
    double Tsn;
    double ebh, abh,mbh=MBH, rperi, r_TDE;
    double periodbh, f0, thismaxradius;
    double star1_pos[3], star1_vel[3];
    double bh_pos[3], bh_vel[3];
    double angle_rv,angle_vv,angle_outplane;
    double sigma_max=SIGMA_MAX;
    int index;
    double  vk, phi_k, omega_k, vk_comp[3];
    
    
    unsigned int Iunbound=0;    
    unsigned int Ntde=0;   
    unsigned int Nstrippedgiants=0;     
    unsigned int Npresninfall=0;
    unsigned int N_plunges=0;
    unsigned int N_stripped_plunges=0;
    unsigned int NH_emris=0;
    unsigned int NL_emris=0;
    unsigned int NH_stripped_emris=0;
    unsigned int NL_stripped_emris=0;
    
    
    for(I=0; I<imax ; I++)
    {
      int stripped=FALSE;
      
      /*** Let's work with masses and stellar evolution timescales **/    
      //zero age main sequence:
      m1i= mass_generator(MMIN1, MMAX, imf_slope-1., gsl_rng_uniform_pos(randnum));
      // mass from ZAMS to pre-sn to remnant:
      change_masses(tfin, mzams, mfin, mremn, m1i, rmax, &m1presn, &m1f, &Tsn, &thismaxradius);
      /**************************************************************/ 


      /*** Orbital parameters about the SMBH ***/
      // -----Eccentricity
      if(alpha_ecc<-12.0)
        ebh=  generate_gaussian_eccentricity_distr(randnum, GAUS_MEAN, GAUS_SIGMA);
      else if(alpha_ecc<=-10.0)
        ebh=-(alpha_ecc+10.);
      else
        ebh=generate_powerlaw_eccentricity_distr(alpha_ecc, gsl_rng_uniform_pos(randnum));
      // -----Semimajor axis
      abh = array[ib];
      /*if((rmax_ic-rmin_ic)/(rmin_ic) < 1.e-6)
        abh=rmax_ic;
      else
        abh=generate_cuspy_profile(rmin_ic, rmax_ic, gamma, gsl_rng_uniform_pos(randnum));*/
      // -- initial pericentre 
      rperi=abh*(1.-ebh);
      /**************************************************************/
      
      
      /********** Check TDE / stripping / Premature infall **********/
      //TDE
      r_TDE=pow((mbh/m1presn),0.3333333333333333)*Rstrip;
      if(rperi<r_TDE)
      {
        Ntde++;
        continue; //do not finish iteration as the star is lost for EMRIs (TDE)
      }
          
      //stripped envelope but no TDE
      if(rperi<pow((mbh/m1presn),0.3333333333333333)*thismaxradius)
      {
        Nstrippedgiants++;
        stripped =TRUE;
        // proceed as most of the star is still in place
      }
      
      // case of pre-SN star undergoes EMRI prior to exploding as SN
      if( TGW(abh/RSCALE_PC, ebh, mbh/MSCALE, m1f/MSCALE)/1.e+6 <Tsn ) // yr to Myr with /1.e+6
      {
        Npresninfall++;
        //if (stripped==TRUE)
        //  Nstrg_premEmri++;    
        continue; //do not finish iteration as the star is lost onto the SMBH prior to SN
      }
      /**************************************************************/
      
      
      /************ GENERATE BINARY AND ADD A KICK ******************/
      // 1 -- binary WITHOUT the kick
      double random=gsl_rng_uniform_pos(randnum);
      binary_generator(I,m1presn, mbh, ebh, abh, random, star1_pos, star1_vel, bh_pos, bh_vel, &periodbh, &f0);
      // ------------- no need for rotation as it is ONE single star
      // 2 -- Compute the SN kick magnitude (here from maxwellian)
      vk=generate_kickvel(sigma_max,gsl_rng_uniform_pos(randnum),gsl_rng_uniform_pos(randnum));
      //-------------- if it is not NS and we want it, BH kick is normalized to its mass:
      if(m1f>3.*MSCALE && (norm_bh_kick==TRUE) ) 
        vk=vk*1.4*(MSCALE)/m1f;
      // 3 --   Compute isotropically distributed angles for kick
      phi_k=asin(2.*gsl_rng_uniform(randnum)-1.); //betw -pi/2 and pi/2
      omega_k=2.*M_PI*gsl_rng_uniform_pos(randnum); //betw 0 and 2pi
      // 4 --  Create the kick vector with the previous quantities
      vk_comp[0]=vk*cos(omega_k)*cos(phi_k);
      vk_comp[1]=vk*sin(omega_k)*cos(phi_k);
      vk_comp[2]=vk*sin(phi_k);
      
      // -------------- Here we check the angles between the kick and other vectors
      angle_rv=angle_between_vectors(star1_pos,vk_comp); //position vector - kick
      angle_vv=angle_between_vectors(star1_vel,vk_comp); // velocity vector - velocity kick
      angle_outplane=asin(vk_comp[2]/vk); // inclination of the kick wrt the initial orbital plane
      //---------------
      
      // 5 --  The kick velocity vector is added to the CO initial velocity
      for(index=0;index<3;index++)
        star1_vel[index]+=vk_comp[index];   
      // 6 --recentering on SMBH (really not necessary)
      for(index=0;index<3;index++)
      {      
        bh_pos[index]-=bh_pos[index];
        star1_pos[index]-=bh_pos[index];
        
        bh_vel[index]-=bh_vel[index];
        star1_vel[index]-=bh_vel[index];
      }
      /**************************************************************/
      
      
      
      /**************** CHECK CO STATUS AFTER THE SN*****************/
      
      // define post-kick variables: separation SMBH-CO in pos and vel space
      double x,y,z,vx,vy,vz;
      x=star1_pos[0]-bh_pos[0];  vx=star1_vel[0]-bh_vel[0];
      y=star1_pos[1]-bh_pos[1];  vy=star1_vel[1]-bh_vel[1];
      z=star1_pos[2]-bh_pos[2];  vz=star1_vel[2]-bh_vel[2];    
      // new variables to compute the final orbital parameters
      double r=sqrt(sq(x)+sq(y)+sq(z));
      double v=sqrt(sq(vx)+sq(vy)+sq(vz));
      double E= 0.5*v*v-mbh/r;
      double Lx=y*vz-z*vy;
      double Ly=z*vx-x*vz;
      double Lz=x*vy-y*vx;
      double L=sqrt(Lx*Lx+Ly*Ly+Lz*Lz);
      double  a, e;
      // 1 --  Check if still bound!
      if(E<0.)
      {
        
        a=-.5*mbh/E;
        e=sqrt(1.-L*L/(a*mbh));
        
        // 2 --  If so, compute the GW timescale and check if shorter than Hubble
        double  tgw=TGW(a/RSCALE_PC, e, mbh/MSCALE, m1f/MSCALE); // in yr
        if(tgw<t_Hubble)
        {
          // 2 --  If so, check if it undergoes a plunge
          int Fp = plunge(a/RSCALE_PC, e,mbh/MSCALE);
          if (Fp == TRUE)
          {
            N_plunges++;
            if (stripped==TRUE)
              N_stripped_plunges++;
            continue; // as EMRI is only a plunge
          }
          // 3 --  Check if NRR is able to deflect it
          // Case H: heavy stellar Bhs with profile having gamma=2
          int deflH = NRR_deflection(a/RSCALE_PC, e, mbh/MSCALE, 2.0, 1.18533254756, 10., 1.e+4);
          int deflL = NRR_deflection(a/RSCALE_PC, e, mbh/MSCALE,1.25, 2.05890778951,  1., 1.e+6);
          if(deflH==FALSE)
          {
            NH_emris++;
            if (stripped==TRUE)
              NH_stripped_emris++;
          }
          if(deflL==FALSE)
          {
            NL_emris++;
            if (stripped==TRUE)
              NL_stripped_emris++;
          }
        }

      }
      // 1b -- If it gets unbound:
      else
      {  
        Iunbound++;
      }
      
      /**************************************************************/
    } 
    Iend=imax;
    
    /***************** FINAL STATISTICS ***************************/
    
    double tde_frac=((double) Ntde)/((double) (Iend)); 
    double stripped_frac=((double) Nstrippedgiants)/((double) (Iend));  
    double premature_gw_frac=((double) Npresninfall)/((double) (Iend));
    double unbound_frac=((double) Iunbound)/((double) (Iend));
    double plunges_frac=((double) N_plunges)/((double) (Iend));
    double strip_plunges_frac=((double) N_stripped_plunges)/((double) (Iend));
    double Hemris_frac=((double) NH_emris)/((double) (Iend));
    double H_stripped_emris_frac=((double) NH_stripped_emris)/((double) (Iend));
    double Lemris_frac=((double) NL_emris)/((double) (Iend));
    double L_stripped_emris_frac=((double) NL_stripped_emris)/((double) (Iend));
    
    //#pragma omp critical
    {    
    printf("%7.2g  ",array[ib]/RSCALE_PC );
    printf("%9.3e  ",tde_frac);
    printf("%9.3e  ",stripped_frac);  
    printf("%9.3e  ",premature_gw_frac);
    printf("%9.3e  ",unbound_frac);    
    printf("%9.3e  ",plunges_frac);  
    printf("%9.3e  ",strip_plunges_frac);
    printf("%9.3e  ",Hemris_frac);
    printf("%9.3e  ",H_stripped_emris_frac);
    printf("%9.3e  ",Lemris_frac);
    printf("%9.3e  ",L_stripped_emris_frac);
    printf(" %lu\n",Iend);
    
    /*printf("%7.2g  ",array[ib]/RSCALE_PC );
    printf("%9u  ",Ntde);
    printf("%9u  ",Nstrippedgiants);  
    printf("%9u  ",Npresninfall);
    printf("%9u  ",Iunbound);    
    printf("%9u  ",N_plunges);  
    printf("%9u  ",N_stripped_plunges);
    printf("%9u  ",NH_emris);
    printf("%9u  ",NH_stripped_emris);
    printf("%9u  ",NL_emris);
    printf("%9u  ",NL_stripped_emris);
    printf(" %lu\n",Iend);*/
    
    fflush(stdout);
    }
    /**************************************************************/
  }  

  exit(EXIT_SUCCESS);
  
}
